#!/usr/bin/env python

__author__ = "Alejandro"
__version__ = "1.1"

import sys
import pprint
from multiprocessing import Process,Pool

N = int(sys.argv[1]) # Matrix lenght
out = [[0]*N for i in range(N)] # The matrix

def loop(x: int, y: int, len: int, cnt: int) -> None:
	""" Fill a matrix's ring of lenght `len` from (x,y) """
	i = x
	for j in range(y, y+len):
		out[i][j] = cnt = cnt+1
	j = y+len-1
	for i in range(x+1, x+len):
		out[i][j] = cnt = cnt+1
	i = x+len-1
	for j in range(y+len-2, y-1, -1):
		out[i][j] = cnt = cnt+1		
	j = y
	for i in range(x+len-2, x, -1):
		out[i][j] = cnt = cnt+1

def main() -> None:
	""" Main function """	
	k = ini = 0
	for n in range(N, 0, -2):
		Process(target=loop, args=(k, k, n, ini)).start()
		ini += 4*(N-(2*k+1))
		k += 1

	if N <= 16:
		pprint.pprint(out)
		
if __name__ == "__main__":
	main()
