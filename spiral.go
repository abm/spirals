//author = "Alejandro"
//version = "0.1"

package main
 
import (
    "os"
    "fmt"
    "strconv"
    "time"
)


func loop(out [][]int, x, y, l, cnt int) {
	// Fill a matrix's ring of lenght `l` from (x,y)
	for i,j := x,y; j < y+l; j++ {
		out[i][j] = cnt
		cnt++
	}
	for i,j := x+1,y+l-1; i < x+l; i++ {
		out[i][j] = cnt
		cnt++
	}
	for i,j := x+l-1,y+l-2; j > y-1; j-- {
		out[i][j] = cnt
		cnt++
	}
	for i,j := x+l-2,y; i > x; i-- {
		out[i][j] = cnt
		cnt++
	}
}

func main() {
	// Main function
	N, _ := strconv.Atoi(os.Args[1]) // Matrix lenght
	out := make([][]int, N)	// The matrix
	for i := range out {
    		out[i] = make([]int, N)
	}

	for k, n, ini := 0, N, 0; n>0; n -= 2 {
		go loop(out, k, k, n, ini)

		ini += 4*(N-(2*k+1))
		k++
	}

	if N <= 16 {
		for i := range out {
			fmt.Println(out[i])	
		}
	}
	
	time.Sleep(15 * time.Second)
}
