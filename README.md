You can then build and run the Docker image:

```bash
docker build -t py-spiral-img -f Docker-py
docker run -it --rm --name py-spiral py-spiral-img <N>
```

Or:

```bash
docker build -t go-spiral-img -f Docker-go
docker run -it --rm --name go-spiral go-spiral-img <N>
``` 

for i in {1000..7000..200}; do echo $i; podman run -it --rm --name spiral py-spiral $i; done
